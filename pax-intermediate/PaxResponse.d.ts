/// <reference types="qs" />
import { CookieOptions, Errback, Request, Response } from 'express';
import { Liquid } from 'liquidjs';
import { LiquidOptions } from 'liquidjs/dist/liquid-options';
export declare class PaxResponse {
    private readonly expressResponse;
    private listeners;
    private liquid;
    private request;
    constructor(expressResponse: Response, liquid: Liquid, request: Request);
    private constructData;
    get _raw(): Response<any>;
    get _request(): Request<import("express-serve-static-core").ParamsDictionary, any, any, import("qs").ParsedQs>;
    get app(): import("express-serve-static-core").Application;
    get headersSent(): boolean;
    get locals(): any;
    private notify;
    listenFor(eventName: string, listener: (name: string, data: {
        [key: string]: any;
    }) => void): void;
    append(field: string, value?: string[] | string): PaxResponse;
    attachment(filename?: string): PaxResponse;
    cookie(name: string, value: any, options?: CookieOptions): PaxResponse;
    clearCookie(name: string, options?: CookieOptions): PaxResponse;
    download(path: string, filename?: string, fn?: Errback): void;
    end(cb?: () => void): void;
    format(object: any): PaxResponse;
    get(field: string): string;
    header(field: string, value?: string | string[]): string | PaxResponse;
    json(body?: any): PaxResponse;
    jsonp(body?: any): PaxResponse;
    links(links: any): PaxResponse;
    location(path: string): PaxResponse;
    redirect(url: string, status?: number): void;
    render(target: string, data?: object, options?: LiquidOptions, alternateLiquid?: Liquid): Promise<PaxResponse>;
    renderFile(file: string, data?: object, alternateLiquid?: Liquid, options?: LiquidOptions): Promise<PaxResponse>;
    send(body?: any): PaxResponse;
    sendFile(path: string, options: any, fn: Errback): void;
    sendStatus(statusCode: number): PaxResponse;
    set(field: string, value?: string | string[]): PaxResponse;
    status(code: number): PaxResponse;
    type(type: string): PaxResponse;
    vary(field: string): PaxResponse;
    private sendTemplate;
    /**
     * Renders the 401.html template, falling back to a basic html response if the file fails to load
     * @return {PaxResponse} this response
     */
    unauthorized(): PaxResponse;
    /**
     * Renders the 403.html template, falling back to a basic html response if the file fails to load
     * @return {PaxResponse} this response
     */
    forbidden(): PaxResponse;
    /**
     * Renders the 404.html template, falling back to a basic html response if the file fails to load
     * @return {PaxResponse} this response
     */
    notFound(): PaxResponse;
}
