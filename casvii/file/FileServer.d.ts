import { NextFunction, Request, Response } from "express";
import { CASComposite } from "../integration/CASComposite";
export declare class FileServer {
    private basePath;
    private readonly composite;
    private records;
    constructor(basePath: string, composite: CASComposite);
    handle(req: Request, res: Response, next: NextFunction): void;
}
