import { PaxResponse } from "../../pax-intermediate/PaxResponse";
import { DomainBundle } from "../../pax-intermediate/PaxDomainRegister";
import { PrefixBundle } from "../../pax-intermediate/PaxNamespaceRegister";
import { PaxEndpointsRegister } from "../../pax-intermediate/PaxEndpointsRegister";
import { Request } from "express";
import { CASComposite } from "./CASComposite";
import { ProvisionalAuthenticationStatus } from "../actions/process/AuthenticationProcessImpl";
export declare type HandlerFunction = (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void;
export declare type OptionType = {
    useJSON?: boolean;
    hide?: boolean;
    requireUnauthenticated?: boolean;
    requirePermissions?: string[];
    requiredProvisionalStatus?: ProvisionalAuthenticationStatus | null;
};
export declare class CASEndpointsRegister {
    private endpointRegister;
    private authenticator;
    constructor(endpointRegister: PaxEndpointsRegister, authenticator: CASComposite);
    private add;
    delete(endpoint: string | RegExp, requiresAuthentication: boolean, handler: HandlerFunction, options: OptionType | undefined, arg: any): void;
    get(endpoint: string | RegExp, requiresAuthentication: boolean, handler: HandlerFunction, options: OptionType | undefined, arg: any): void;
    head(endpoint: string | RegExp, requiresAuthentication: boolean, handler: HandlerFunction, options: OptionType | undefined, arg: any): void;
    post(endpoint: string | RegExp, requiresAuthentication: boolean, handler: HandlerFunction, options: OptionType | undefined, arg: any): void;
    put(endpoint: string | RegExp, requiresAuthentication: boolean, handler: HandlerFunction, options: OptionType | undefined, arg: any): void;
}
