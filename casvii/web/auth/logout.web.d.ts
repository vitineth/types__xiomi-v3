import { Endpoint } from "../Endpoint";
import { CASComposite } from "../../integration/CASComposite";
import { Request } from "express";
import { PaxResponse } from "../../../pax-intermediate/PaxResponse";
import { DomainBundle } from "../../../pax-intermediate/PaxDomainRegister";
import { PrefixBundle } from "../../../pax-intermediate/PaxNamespaceRegister";
export declare namespace Logout {
    class LogoutAPI extends Endpoint {
        constructor(composite: CASComposite);
        handle(request: Request, response: PaxResponse, domain: DomainBundle, namespace: PrefixBundle): Promise<any>;
    }
}
