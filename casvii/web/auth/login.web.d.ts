import { Endpoint } from "../Endpoint";
import { CASComposite } from "../../integration/CASComposite";
import { Request } from "express";
import { PaxResponse } from "../../../pax-intermediate/PaxResponse";
import { DomainBundle } from "../../../pax-intermediate/PaxDomainRegister";
import { PrefixBundle } from "../../../pax-intermediate/PaxNamespaceRegister";
import { Liquid } from "liquidjs";
export declare namespace Login {
    class LoginWeb extends Endpoint {
        private renderer;
        constructor(composite: CASComposite, renderer: Liquid);
        handle(request: Request, response: PaxResponse, domain: DomainBundle, namespace: PrefixBundle): Promise<any>;
    }
    class LoginAPI extends Endpoint {
        constructor(composite: CASComposite);
        handle(request: Request, response: PaxResponse, domain: DomainBundle, namespace: PrefixBundle): Promise<any>;
    }
}
