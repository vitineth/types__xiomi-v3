import { CASComposite } from "../integration/CASComposite";
import { Request } from "express";
import { PaxResponse } from "../../pax-intermediate/PaxResponse";
import { DomainBundle } from "../../pax-intermediate/PaxDomainRegister";
import { PrefixBundle } from "../../pax-intermediate/PaxNamespaceRegister";
import { ProvisionalAuthenticationStatus } from "../actions/process/AuthenticationProcessImpl";
export declare enum AuthenticationRequirement {
    AUTHENTICATED = 0,
    UNAUTHENTICATED = 1
}
export declare enum Method {
    DELETE = "delete",
    GET = "get",
    HEAD = "head",
    POST = "post",
    PUT = "put"
}
export declare abstract class Endpoint {
    protected _composite: CASComposite;
    private _method;
    private _url;
    private _isAPI;
    private _hiddenOnUnauthenticated;
    private _permissions;
    private _requirement;
    private _requiredProvisionalStatus;
    protected constructor(composite: CASComposite, method: Method, url: string | RegExp, isAPI: boolean, requirement: AuthenticationRequirement, hiddenOnUnauthenticated?: boolean, permissions?: string[] | undefined, provisional?: ProvisionalAuthenticationStatus | null);
    abstract handle(request: Request, response: PaxResponse, domain: DomainBundle, namespace: PrefixBundle): any;
    get method(): Method;
    get url(): string | RegExp;
    get isAPI(): boolean;
    get hiddenOnUnauthenticated(): boolean;
    get permissions(): string[] | undefined;
    get requirement(): AuthenticationRequirement;
    get requiredProvisionalStatus(): ProvisionalAuthenticationStatus | null;
}
