import { Endpoint } from "../Endpoint";
import { CASComposite } from "../../integration/CASComposite";
import { Liquid } from "liquidjs";
import { Request } from "express";
import { PaxResponse } from "../../../pax-intermediate/PaxResponse";
import { DomainBundle } from "../../../pax-intermediate/PaxDomainRegister";
import { PrefixBundle } from "../../../pax-intermediate/PaxNamespaceRegister";
export declare namespace ConfirmPasswordChange {
    class ConfirmPasswordChangeWeb extends Endpoint {
        private renderer;
        constructor(composite: CASComposite, renderer: Liquid);
        handle(request: Request, response: PaxResponse, domain: DomainBundle, namespace: PrefixBundle): Promise<any>;
    }
    class ConfirmPasswordChangeAPI extends Endpoint {
        constructor(composite: CASComposite);
        handle(request: Request, response: PaxResponse, domain: DomainBundle, namespace: PrefixBundle): Promise<any>;
    }
}
