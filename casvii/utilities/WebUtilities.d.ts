import { PaxResponse } from "../../pax-intermediate/PaxResponse";
import { CASResponse } from "./CASResponse";
import { Request } from "express";
import { RequestSession } from "./session/impl/RequestSession";
export declare namespace RequestUtilities {
    /**
     * An internal type for the body of any requests from the web server, cleans up the definitions of the functions.
     */
    type RequestBody = {
        [key: string]: any;
    };
    /**
     * Tests whether the given data object contains all the keys in {@code required} and only those listed in
     * {@code required} and {@code optional}. Will return a string on fail with the name of the invalid key or the missing
     * key in the format {@code Contains invalid key []} or {@code Missing required key []}.
     * @param data the data object to test
     * @param required the keys which must be in the data object
     * @param optional the set of keys which can optionally be present
     */
    function requestContainsKeys(data: RequestBody, required: string[], optional: string[]): boolean | string;
    function requestToSession(req: Request): RequestSession;
}
export declare namespace ResponseUtilities {
    function sendDefaultJSONResponse(response: CASResponse, res: PaxResponse): void;
    /**
     * Sends the default internal server error using JSON is it is an API
     * @param response the response through which this should be written
     * @param isAPI if the response should be provided in JSON
     */
    function sendDefaultInternalServerError(response: PaxResponse, isAPI: boolean): void;
}
