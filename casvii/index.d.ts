import { CASComposite } from "./integration/CASComposite";
import { RequestHandler } from "express";
import { PaxDomainRegister } from "../pax-intermediate/PaxDomainRegister";
import { PermissionDAO } from "./database/dao/PermissionDAO";
import { UserDAO } from "./database/dao/UserDAO";
import { TwoFactorDAO } from "./database/dao/TwoFactorDAO";
import { TokenDAO } from "./database/dao/TokenDAO";
import { SecurityQuestionDAO } from "./database/dao/SecurityQuestionDAO";
import { LoggingDAO } from "./database/dao/LoggingDAO";
export declare type DaoCollection = {
    loggingDAO: LoggingDAO;
    securityQuestionDAO: SecurityQuestionDAO;
    tokenDAO: TokenDAO;
    twoFactorDAO: TwoFactorDAO;
    userDAO: UserDAO;
    permissionDAO: PermissionDAO;
};
export declare class CASVii {
    private mailer;
    composite: CASComposite;
    private _renderer;
    private constructor();
    register(app: PaxDomainRegister, csrf: RequestHandler): void;
    static build(): Promise<CASVii>;
}
