import { IPasswordProcesses } from "../interface/IPasswordProcesses";
import { UserDAO } from "../../database/dao/UserDAO";
import { TokenDAO } from "../../database/dao/TokenDAO";
import { LoggingDAO } from "../../database/dao/LoggingDAO";
import { CASEmailProvider } from "../../mail/CASEmailProvider";
export declare namespace PasswordProcessImpl {
    const construct: (mailer: CASEmailProvider, loggingDAO: LoggingDAO, userDAO: UserDAO, tokenDAO: TokenDAO) => IPasswordProcesses;
}
