import { ToolError } from "../errors/ToolError";
import { LoggingDAO } from "../database/dao/LoggingDAO";
import { CASResponse } from "../utilities/CASResponse";
export declare class ProcessImpl {
    private logging;
    constructor(logging: LoggingDAO);
    passthroughLog(code: number, internal: string, external: string, data?: {
        [key: string]: any;
    } | undefined): Promise<CASResponse>;
    verifyDataElements(data: {
        [key: string]: any;
    }, requiredKeys: string[], optionalKeys?: string[]): Promise<CASResponse | undefined>;
    /**
     * Runs the given tool (awaiting it) through the action function. On success it will return a passthrough log
     * returning 200 and logging that the action name was done successfully, logging an additional data. On error, it
     * automatically log any non-tool errors but will dispatch to the handlers if one is found for the error identifier.
     * @param action the action to execute (expected to be an async function as it is awaited) with the result discarded
     * @param actionName the name of the tool that is being run in a human readable format
     * @param publicSuccess the message to log on success
     * @param additional any additional data to log when performing any log actions
     * @param handlers an object of handlers for tool errors
     */
    runTool(action: () => void, actionName: string, publicSuccess: string, additional: any, handlers: {
        [key: string]: (e: ToolError) => Promise<CASResponse>;
    }): Promise<CASResponse>;
}
export declare class HttpStatusCode {
    static readonly SUCCESS: number;
    static readonly REDIRECT: number;
    static readonly BAD_REQUEST: number;
    static readonly UNAUTHORIZED: number;
    static readonly FORBIDDEN: number;
    static readonly NOT_FOUND: number;
    static readonly INTERNAL_SERVER_ERROR: number;
    static readonly SERVICE_UNAVAILABLE: number;
}
