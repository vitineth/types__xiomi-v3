export declare function verifyRequired(): boolean;
export declare function loadEasy(path: string, priority?: number): any;
export declare function load(path: string, priority?: number): any;
export declare function has(path: string): boolean;
export declare function get(path: string): any;
